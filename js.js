function addPush (capt) {
    var push = document.createElement("div")
    push.classList.add("push")
    var caption = document.createElement("h3")
    caption.innerHTML = "Как прошел ваш день?"
    var discription = document.createElement("p")
    discription.innerHTML = capt
    push.appendChild(caption)
    push.appendChild(discription)
    var cont = document.querySelector(".cont")
    cont.appendChild(push) 
    setTimeout(() => {
        push.remove()
    }, 3000) 
    setTimeout(()  => {
        push.classList.add("remove")
    }, 2000)
}

var forma = document.querySelector("form")
forma.addEventListener("submit", function(e){
    e.preventDefault()
    var inp = document.querySelector("[name=clientText]").value
    addPush(inp)
})

